
<html>
<head>
<title>News Info Website </title>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="tabcontent.css" />
<script type="text/javascript" src="tabcontent.js">
</script>

</head>
<body>
<div id="templatemo_container">

	<div id="templatemo_top_panel">
        <div id="templatemo_sitetitle">
            NEWS INFO WEBSITE
        </div>
              <p align="rignt"><div id="templatemo_searchbox">
            <form action="#" method="get">
            <input type="text" name="q" size="10" id="searchfield" title="searchfield" />
            <input type="submit" name="Search" value="Search" alt="Search" id="searchbutton" title="Search" />
            <br /></p>
            </form>
        </div>
	</div> 
    
    <div id="templatemo_menu">
        <ul>
            <li><a href="home.php"  class="current">Home</a></li>
            <li><a href="dailynews.php">Daily News</a></li>
            <li><a href="sports.php">Sports</a></li>
            <li><a href="Business.php">Business</a></li>  
            <li><a href="Politics.php">Politics</a></li> 
            <li><a href="Entertainment.php">Entertainment</a></li>                     
            <li><a href="usersignup.php">Sign Up</a></li>            
        </ul> 
	</div>
    
	<div id="templatemo_content">

   	  <div id="templatemo_main_leftcol">
        	<div class="templatemo_leftcol_subcol">
            
           	  <div id="templatemo_topnews">
                  <img src="images1/56.jpg" alt="image" width="600" height="300" />
                    <p align="justify">
                <p></div>
           	  <p align="justify"><div class="para">The Indian cricket team, also known as Team India and Men in Blue, is the national cricket team of India. Governed by the Board of Control for Cricket in India (BCCI), it is a full member of the International Cricket Council (ICC) with Test and One Day International (ODI) status.Although cricket was introduced to India by European merchant sailors in the 18th century, and the first cricket club in India was established in Calcutta in 1792, India's national cricket team did not play its first Test match until 25 June 1932 at Lord's.[3] They became the sixth team to be granted Test cricket status.[4] In its first fifty years of international cricket, India was one of the weaker teams in international cricket, winning only 35 of the 196 Test matches it played during the period.[5] The team, however, gained strength in the 1970s with the emergence of players such as batsmen Sunil Gavaskar and Gundappa Viswanath, all-rounder Kapil Dev and the Indian spin quartet – Erapalli Prasanna and Srinivas Venkataraghavan (both off spinners), Bhagwat Chandrasekhar (a leg spinner), and Bishen Singh Bedi (a left-arm spinner). Traditionally much stronger at home than abroad, the Indian team has improved its overseas form since the start of the 21st century, winning Test matches in Australia, England and South Africa. It has won the Cricket World Cup twice – in 1983 under the captaincy of Kapil Dev and in 2011 under Mahendra Singh Dhoni's captaincy. After winning the 2011 World Cup, India became only the third team after West Indies and Australia to have won the World Cup more than once,[6] and the first cricket team to win the World Cup at home. It has won the 2007 ICC World Twenty20 and 2013 ICC Champions Trophy, under the captaincy of Dhoni. It was also the joint champions of 2002 ICC Champions Trophy, along with Sri Lanka.

As of 29 March 2015, the Indian cricket team is ranked seventh in Tests, second in ODIs and second in T20Is by the ICC.[1] Virat Kohli is the current captain of the team in Tests while Dhoni is the ODI and T20I captain. The Indian cricket team has rivalries with other Test-playing nations, most notably with Pakistan, the political arch-rival of India. However in recent times, rivalries with nations like Australia, England and South Africa have also gained prominence. </div></p>
        </div>
   	  </div> 
        
      <div id="templatemo_main_rightcol">
       	<div class="templatemo_rcol_sectionwithborder">
            	<div id="templatemo_video_section">
                    <h2>Latest Sports Videos</h2>
                    <div class="video_box">
                        <img src="images1/templatemo_image_09.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Lorem ipsum dolor sit amet</a>
                    </div>
                    <div class="video_box">
                        <img src="images1/templatemo_image_10.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Nunc quis sem nec tincidunt</a>                    
                    </div> 
                    <div class="video_box">
                        <img src="images1/templatemo_image_11.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Duis vitae velit sed dui</a>                    
                    </div>
                    <div class="view_all"><a href="#">View All</a></div>
				</div>
        </div>
        	<div id="templatemo_gallery_section2">
        	  <h2>Today's Sports Images</h2>
        	  <div id="image_section2"> <a href="#"><img src="images1/34.png" alt="image" width="386" height="201" /></a> <a href="#"><img src="images1/44.jpg" alt="image" width="383" height="203" /></a></div>
        	  <div class="view_all"><a href="#">View All Photo</a></div>
   	    </div>
      </div>
  </div></div></body>
</html>