
<html>
<head>
<title>News Info Website </title>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="tabcontent.css" />
<script type="text/javascript" src="tabcontent.js">
</script>

</head>
<body>
<div id="templatemo_container">

	<div id="templatemo_top_panel">
        <div id="templatemo_sitetitle">
            NEWS INFO WEBSITE
        </div>
              <p align="rignt"><div id="templatemo_searchbox">
            <form action="#" method="get">
            <input type="text" name="q" size="10" id="searchfield" title="searchfield" />
            <input type="submit" name="Search" value="Search" alt="Search" id="searchbutton" title="Search" />
            <br /></p>
            </form>
        </div>
	</div> 
    
    <div id="templatemo_menu">
        <ul>
            <li><a href="home.php"  class="current">Home</a></li>
            <li><a href="dailynews.php">Daily News</a></li>
            <li><a href="sports.php">Sports</a></li>
            <li><a href="Business.php">Business</a></li>  
            <li><a href="Politics.php">Politics</a></li> 
            <li><a href="Entertainment.php">Entertainment</a></li>                     
            <li><a href="usersignup.php">Sign Up</a></li>            
        </ul> 
	</div>
    
	<div id="templatemo_content">

   	  <div id="templatemo_main_leftcol">
        	<div class="templatemo_leftcol_subcol">
            
            	<div id="templatemo_topnews">
                  <img src="images1/651-the-year.jpg" alt="image" width="600" height="300" />
                    <p align="justify">
                <p></div> 
                
            <div id="templatemo_topnews" align="right" width="1000%" height="300"  > <p>New Delhi: Rahul Gandhi, who is on a long sabbatical, will be back in action to attend a Kisan rally here on April 19 against land ordinance brought out by the Narendra Modi government.
The rally is significant as it is being held on the eve of the second phase of the Budget session of Parliament at a time when almost the entire opposition has joined ranks against the ordinance, which is being dubbed by them as "anti-farmer and pro-corporate".
Lakhs of farmers from all over the country are expected to attend the rally, being held at the historic Ramlila ground in the national capital, which Congress is planning to turn into a mega event seeking to exploit the strong sentiments in the countryside over the issue.
Party general secretary Digvijay Singh told reporters after a meeting of AICC General Secretary, PCC Chiefs and CLP leaders presided by party veteran A K Antony.
Singh said lakhs of farmers are expected to participate in the protest action with several states planning to bring large contingents, especially those bordering Delhi like Uttar Pradesh, Haryana and Rajasthan.
The party had earlier booked Ramlila ground for April 12 but postponed the event by a week.
Congress President Sonia Gandhi will address the rally as also all prominent leaders of the party, Singh said.
Asked whether Rahul Gandhi, who continues to be on a month -long sabbatical, will be attending, Singh remarked "all senior leaders will participate in it. Is he not among senior leaders? Rahul is also among the senior leaders".
Congress chief had last week rejected government's offer for a dialogue, saying it was a "mockery" as the BJP regime had unilaterally imposed the land ordinance.
Dubbing the bill as anti-farmer, she had accused the government of "bending over backwards" to favour industrialists. </p>
                
                

                    <p align="justify" >
                <p></div> 
            </div>
   	  </div> 
        
      <div id="templatemo_main_rightcol">
       	<div class="templatemo_rcol_sectionwithborder">
            	<div id="templatemo_video_section">
                    <h2>Latest Videos</h2>
                    <div class="video_box">
                        <img src="images1/templatemo_image_09.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Lorem ipsum dolor sit amet</a>
                    </div>
                    <div class="video_box">
                        <img src="images1/templatemo_image_10.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Nunc quis sem nec tincidunt</a>                    
                    </div> 
                    <div class="video_box">
                        <img src="images1/templatemo_image_11.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Duis vitae velit sed dui</a>                    
                    </div>
                    <div class="view_all"><a href="#">View All</a></div>
				</div>
            </div>
        	<div id="templatemo_gallery_section2">
        	  <h2>Today's Images</h2>
        	  <div id="image_section2"> <a href="#"><img src="images1/modi.jpg" alt="image" /></a> <a href="#"><img src="images1/67.jpg" alt="image" /></a> <a href="#"><img src="images1/86.jpg" alt="image" /></a></div>
        	  <div class="view_all"><a href="#">View All Photo</a></div>
      	  </div>
      </div>
  </div></div></body>
</html>