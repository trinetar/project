
<html>
<head>
<title>News Info Website </title>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="tabcontent.css" />
<script type="text/javascript" src="tabcontent.js">
</script>

</head>
<body>
<div id="templatemo_container">

	<div id="templatemo_top_panel">
        <div id="templatemo_sitetitle">
            NEWS INFO WEBSITE
        </div>
              <p align="rignt"><div id="templatemo_searchbox">
            <form action="#" method="get">
            <input type="text" name="q" size="10" id="searchfield" title="searchfield" />
            <input type="submit" name="Search" value="Search" alt="Search" id="searchbutton" title="Search" />
            <br /></p>
            </form>
        </div>
	</div> 
    
    <div id="templatemo_menu">
        <ul>
            <li><a href="home.php"  class="current">Home</a></li>
            <li><a href="dailynews.php">Daily News</a></li>
            <li><a href="sports.php">Sports</a></li>
            <li><a href="Business.php">Business</a></li>  
            <li><a href="Politics.php">Politics</a></li> 
            <li><a href="Entertainment.php">Entertainment</a></li>                     
            <li><a href="usersignup.php">Sign Up</a></li>            
        </ul> 
	</div>
    
	<div id="templatemo_content">

   	  <div id="templatemo_main_leftcol">
        	<div class="templatemo_leftcol_subcol">
            
            	<div id="templatemo_topnews">
                  <img src="images1/local-news.jpg" alt="image" width="610" height="300" />
                    <p align="justify">
                <p></div> <img src="images1/k.jpg" alt="image" width="538" height="325"  />
           <p width="99000" position="absolute" >Kashmir floods: </p>
          					Toll rises to 16assixmorebodiesrecoveredfromandslidesSrinagar/Jammu:Sixbodies were recovered on Tuesday inBudgamdistrict taking the death toll to 16 in Jammu  and Kashmir as several areas of the Valley and parts of Jammu were in deluge caused by heavy rain over the weekend.
             The six bodies were recovered on Tuesday from the debris in Laden village in Chadoora of Budgam, a senior police official said, adding that one person who was trapped in mudslides was feared dead.
             See in Pics: Flood creates havoc after heavy rains in Kashmir
             
             Process to evacuate affected people was underway as eight teams of National Disaster Response Force (NDRF) were rushed to Kashmir.
             Armed forces, along with four helicopters, have been placed in readiness for deployment at short notice after 
             local authorities declared a flood situation in the Valley.Bodies of all the 16 persons, who were killed in a house collapse at village Laden in Chadoora tehsil, were recovered from the debris.
             
             The house was hit by a landslide during the intervening night of March 29 and 30. The village has been witnessing land erosion after the recent rains.
             
             The deceased included nine females and six males belonging to two families. Ghulam Nabi Hajam was head of one family. He too lost his life in the mishap. His wife Zaina, four sons Riyaz Ahmad, Bilal Ahmad, Mohammad Iqbal and Faisal and five daughters Shahida, Naseema, Bitta, Fareeda and Shageena were also killed in the house collapse.
             
             The head of another family Mohammad Shaban Hajam also died in the mishap. His wife Rukhsana, sister Naseema, daughter Sameena and two month old girl child were also killed.
             
             Reports said that five family members of Mohammad Shaban Hajam left their unsafe house during the late evening hours of March 29 and shifted into the house of Ghulam Nabi Hajam. However, the house collapsed during the night, killing 16 persons. Four other houses also collapsed but there was no loss of life as nobody was living there at the time of mishap.
             
             As the news about the house collapse spread people rushed to the affected area. They were joined by the officials of civil administration, police and security forces. Minister for agriculture, Ghulam Nabi Lone Hanjura, also reached the spot. Six bodies were recovered till evening and the rest later.
             
             A pall of gloom descended the village and its adjoining areas due the death of 16 persons in the mishap. People in large number visited the affected village. The last rites of the deceased were alsoperformed.
           </p>
                
                

                   
            </div>
   	  </div> 
        
      <div id="templatemo_main_rightcol">
       	<div class="templatemo_rcol_sectionwithborder">
            	<div id="templatemo_video_section">
                    <h2>Latest  LocalVideos</h2>
                    <div class="video_box">
                        <img src="images1/templatemo_image_09.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Lorem ipsum dolor sit amet</a>
                  </div>
                    <div class="video_box">
                        <img src="images1/templatemo_image_10.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Nunc quis sem nec tincidunt</a>                    
                    </div> 
                    <div class="video_box">
                        <img src="images1/templatemo_image_11.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Duis vitae velit sed dui</a>                    
                    </div>
                    <div class="view_all"><a href="#">View All</a></div>
				</div>
            </div>
        	<div id="templatemo_gallery_section2">
        	  <h2>Today's Local Images</h2>
        	  <div id="image_section2"> <a href="#"><img src="images1/amar-ujala.jpg" alt="image" /></a> <a href="#"><img src="images1/news-india-times.jpg" alt="image" /></a></div>
        	  <div class="view_all"><a href="#">View All Photo</a></div>
      	  </div>
      </div>
  </div></div></body>
</html>