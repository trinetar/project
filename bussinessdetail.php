
<html>
<head>
<title>News Info Website </title>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="tabcontent.css" />
<script type="text/javascript" src="tabcontent.js">
</script>

</head>
<body>
<div id="templatemo_container">

	<div id="templatemo_top_panel">
        <div id="templatemo_sitetitle">
            NEWS INFO WEBSITE
        </div>
              <p align="rignt"><div id="templatemo_searchbox">
            <form action="#" method="get">
            <input type="text" name="q" size="10" id="searchfield" title="searchfield" />
            <input type="submit" name="Search" value="Search" alt="Search" id="searchbutton" title="Search" />
            <br /></p>
            </form>
        </div>
	</div> 
    
    <div id="templatemo_menu">
        <ul>
            <li><a href="home.php"  class="current">Home</a></li>
            <li><a href="dailynews.php">Daily News</a></li>
            <li><a href="sports.php">Sports</a></li>
            <li><a href="Business.php">Business</a></li>  
            <li><a href="Politics.php">Politics</a></li> 
            <li><a href="Entertainment.php">Entertainment</a></li>                     
            <li><a href="usersignup.php">Sign Up</a></li>            
        </ul> 
	</div>
    
	<div id="templatemo_content">

   	  <div id="templatemo_main_leftcol">
        	<div class="templatemo_leftcol_subcol">
            
            	<div id="templatemo_topnews">
                  <img src="images1/99.jpg" alt="image" width="600" height="300" />
                    <p align="justify">
                <p></div> 
                 <img src="images1/bb1.jpg" alt="image"  />
            <div id="templatemo_topnews" align="right" width="600"  > <p>India plans to pull its tariff regime closer in line with global norms to prepare for new regional trade pacts being negotiated by advanced economies, the government said on Wednesday.

India has not been invited to join pacts such as the U.S.-led 12 country Trans-Pacific Partnership (TPP) and is "not in a position to join," partly because its tariffs are not competitive, a top official said at the unveiling of a new five year trade policy.

"If the country is to stand up to these agreements, it's important that we start to address these issues," Trade Secretary Rajeev Kher said, adding that India's access to markets was likely to erode when such pacts take effect.

Kher said India needed lower tariffs for intermediate goods to help it further integrate with global supply chains, and that these industries would have to come more competitive. He did not give more details.

Regional trade pacts are being promoted by advanced economies after years of failure to negotiate a global agreement under the World Trade Organisation.

TPP would link a dozen Asia-Pacific economies by eliminating trade barriers and harmonising regulations in a pact covering two-fifths of the world economy and a third of all global trade.

China, which is not part of the TTP negotiations, is pushing for a separate trade liberalization framework.

"They have been explicit about the fact that there are these mega agreements that we are not invited to - as a response to that they are trying to fix things internally," said Akshay Mathur, head of research at foreign policy think tank Gateway House.

Kher said India was interested in a third grouping known as RCEP that combines Southeast Asian nations and six others -Australia, China, India, Japan, New Zealand, and South Korea.

"India expects to be a major beneficiary of the ASEAN Plus six trade pact for which negotiations are likely to be completed by year end," Kher said.

Experts say the viability of that grouping may depend on India's progress in easing domestic regulations and external barriers that constrain economic activity.

India aims to raise its exports to $900 billion by fiscal year 2019/20, the government said in a statement. </p>
                
                

                    <p align="justify" >
                <p></div> 
            </div>
   	  </div> 
        
      <div id="templatemo_main_rightcol">
       	<div class="templatemo_rcol_sectionwithborder">
            	<div id="templatemo_video_section">
                    <h2>Latest Business Videos</h2>
                    <div class="video_box">
                        <img src="images1/templatemo_image_09.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Lorem ipsum dolor sit amet</a>
                  </div>
                    <div class="video_box">
                        <img src="images1/templatemo_image_10.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Nunc quis sem nec tincidunt</a>                    
                    </div> 
                    <div class="video_box">
                        <img src="images1/templatemo_image_11.jpg" alt="image" />
                        <a href="#"><span>Play:</span> Duis vitae velit sed dui</a>                    
                    </div>
                    <div class="view_all"><a href="#">View All</a></div>
				</div>
            </div>
        	<div id="templatemo_gallery_section2">
        	  <h2>Today's Business Images</h2>
        	  <div id="image_section2"> <a href="#"><img src="images1/bb.jpg" alt="image" /></a> <a href="#"><img src="images1/mm.jpg" alt="image" /></a></div>
        	  <div class="view_all"><a href="#">View All Photo</a></div>
      	  </div>
      </div>
  </div></div></body>
</html>